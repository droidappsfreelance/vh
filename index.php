<?php
/**
 * Simple example of web service
 * @author R. Bartolome
 * @version v1.0
 * @return JSON messages with the format:
 * {
 * 	"code": mandatory, string '0' for correct, '1' for error
 * 	"message": empty or string message
 * 	"data": empty or JSON data
 * }
 *
 * This file can be tested from the browser:
 * http://localhost/webservice-php-json/service_test.php
 *
 * Based on
 * http://www.raywenderlich.com/2941/how-to-write-a-simple-phpmysql-web-service-for-an-ios-app
 */

// the API file
require_once 'api.php';

// creates a new instance of the api class
$api = new api();

// message to return
$message = array();

$json_data= file_get_contents('php://input');


$input_data=json_decode($json_data,true);




 $method=$input_data['action'];


switch($method)
{

	case 'get':



	$ObjectType=$input_data['type'];
		switch ($ObjectType) {
		case 'pt':
			$params = array();
		$params['email'] = isset($input_data["email"]) ? $input_data["email"] : '';
		$params['password'] = isset($input_data["password"]) ? $input_data["password"] : '';
		if (is_array($data = $api->getUser($params))) {

			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = "logged in successfully";
			if(sizeof($data)>0){
				$message["data"] = $data;
			}else{
				
				$message["data"]=$data;
			}
			
		} else {
			$message["code"] = "1";
			$message["message"] = "Error on get method";
		}
			break;


			case 'doc':
			$params = array();
		$params['email'] = isset($input_data["email"]) ? $input_data["email"] : '';
		$params['password'] = isset($input_data["password"]) ? $input_data["password"] : '';
		if (is_array($data = $api->getDocter($params))) {

			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = "logged in successfully";
			if(sizeof($data)>0){
				$message["data"] = $data;
			}else{
			
				$message["data"]=$data;
			}
			
		} else {
			$message["code"] = "1";
			$message["message"] = "Error on get method";
		}
			break;

			case 'dp':
			
		if (is_array($data = $api->getAllDep())) {
			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = "";
			if(sizeof($data)>0){
				$message["data"] = $data;
			}else{
				
				$message["data"]=$data;
			}
		} else {
			$message["code"] = "1";
			$message["message"] = "Error on get method";
		}
			break;

			case 'appointment':
			
			 $params['dr_id'] = isset($input_data["dr_id"]) ? $input_data["dr_id"] : '';
		if (is_array($data = $api->getAllAppointmentByDrID($params))) {
			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = "";
			if(sizeof($data)>0){
				$message["data"] = $data;
			}else{
				
				$message["data"]=$data;
			}
		} else {
			$message["code"] = "1";
			$message["message"] = "Error on get method";
		}
			break;
		
		default:
			# code...
			break;
	}
		
		break;


		case 'post':

		//$params = array();
		$ObjectType=$input_data['type'];
		switch ($ObjectType) {
			case 'pt':
			$data = $api->savePatient($input_data);
			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = "Registered successfully";
			$message["data"] = $data;
				break;
			
				case 'doc':
				$data = $api->saveUser($input_data);
			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = "Registered successfully";
			$message["data"] = $data;
				break;
			

			case 'appointment':
				$data = $api->bookAppointment($input_data);
			$message["code"] = "200";
			$message["status"] = "true";
			$message["message"] = " Appointment booked successfully";
			$message["data"] = $data;
				break;
			
			default:
				# code...
				break;
		}
			
	
		break;



case 'getDoc':

		//$params = array();

			if (is_array($data = $api->getAllDoctorsByDeptID($input_data))) {
			$message["code"] = "0";
			$message["data"] = $data;
		} else {
			$message["code"] = "1";
			$message["message"] = "Error on get method";
		}
	
		break;



	default:
		$message["code"] = "1";
		$message["message"] = "Unknown method " . $_POST["action"];
		break;
}

//the JSON message
header('Content-type: application/json; charset=utf-8');
echo json_encode($message);

?>
