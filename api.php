<?php
/**
 * API class
 * @author Rob
 * @version 2015-06-22
 */


 
 require_once 'firebase.php';
 require_once 'push.php';
class api
{
	private $db;

	/**
	 * Constructor - open DB connection
	 *
	 * @param none
	 * @return database
	 */
	function __construct()
	{
		$conf = json_decode(file_get_contents('configuration.json'), TRUE);
		
		$this->db = new mysqli($conf["host"], $conf["user"], $conf["password"], $conf["database"]);


		}

	/**
	 * Destructor - close DB connection
	 *
	 * @param none
	 * @return none
	 */
	function __destruct()
	{
		$this->db->close();
	}

	/**
	 * Get the list of users
	 *
	 * @param none or user id
	 */
	function getUser($params)
	{
		// echo $query = 'SELECT p.id AS id'
		// . ', p.Name AS Name'
		// . ', p.email AS email'
		// . ', p.password AS password'
		// . ', p.mobile AS mobile'
		// . ' FROM VH.pt AS p'
		// . ($params['email'] == ''? '' : ' WHERE p.email = \'' . $this->db->real_escape_string($params['email']) . '\'')
		// . ' ORDER BY p.Name'
		// ;


		$query="SELECT * FROM hospital.patient WHERE email = '".$params['email']."' AND  password = '".$params['password']."'";
		$list = array();
		 $result = $this->db->query($query);
		


		while ($row = $result->fetch_assoc())
		{
			$list[] = $row;
		}
		return $list;
	}



	function getDocter($params)
	{
		// echo $query = 'SELECT p.id AS id'
		// . ', p.Name AS Name'
		// . ', p.email AS email'
		// . ', p.password AS password'
		// . ', p.mobile AS mobile'
		// . ' FROM VH.pt AS p'
		// . ($params['email'] == ''? '' : ' WHERE p.email = \'' . $this->db->real_escape_string($params['email']) . '\'')
		// . ' ORDER BY p.Name'
		// ;


		$query="SELECT * FROM hospital.doctor WHERE email = '".$params['email']."' AND  password = '".$params['password']."'";
		$list = array();
		 $result = $this->db->query($query);
		

 

		while ($row = $result->fetch_assoc())
		{
			$list[] = $row;
		}
		return $list;
	}




	function saveUser($data)
	{
		

		 $name = $data['name'];
		
		 $dp_id = $data['dp_id'];
		 $exp = $data['exp'];
		 $address = $data['address'];
		 $mobile = $data['mobile'];
		 $email = $data['email'];
		  $password = $data['password'];
		    $gcm_regId = $data['gcm_regId'];


$conf = json_decode(file_get_contents('configuration.json'), TRUE);
		
		
$con=mysqli_connect($conf["host"], $conf["user"], $conf["password"], $conf["database"]);

	if(mysqli_query($con,"INSERT INTO hospital.doctor (Name, dp_id, exp,address,mobile,email,password,gcm_regId) VALUES ('$name','$dp_id','$exp','$address','$mobile','$email','$password','$gcm_regId')")==1){
		

		$list = array();	
		
	}






		//echo $result;
			mysqli_close($con);
		return $list;
	}


function savePatient($data)
	{
		

		 $name = $data['name'];
		 $active = $data['active'];
		 $password = $data['password'];
		 $mobile = $data['mobile'];
		  $email = $data['email'];
		   $gcm_regId = $data['gcm_regId'];


$conf = json_decode(file_get_contents('configuration.json'), TRUE);
		
		
$con=mysqli_connect($conf["host"], $conf["user"], $conf["password"], $conf["database"]);
		

	if(mysqli_query($con,"INSERT INTO hospital.patient (Name, email, mobile,password,active,gcm_regId) VALUES ('$name','$email','$mobile','$password','$active','$gcm_regId')")==1){
		

		$list = array();
          
       	
		
	}

		//echo $result;
			mysqli_close($con);
		return $list;
	}



function bookAppointment($data)
	{
		

		 $dp_id = $data['dp_id'];
		 $dr_id = $data['dr_id'];
		 $patient_id = $data['patient_id'];
		 $time_slot = $data['time_slot'];
		


$conf = json_decode(file_get_contents('configuration.json'), TRUE);
		
		
$con=mysqli_connect($conf["host"], $conf["user"], $conf["password"], $conf["database"]);
		

	if(mysqli_query($con,"INSERT INTO hospital.appointment (dr_id, dp_id, patient_id,time_slot) VALUES ('$dr_id','$dp_id','$patient_id','$time_slot')")==1){
		

		$list = array();

		echo $query = 'SELECT * from hospital.patient where id='. '' .$patient_id;
		
		$result = $this->db->query($query);
		$row = $result->fetch_assoc();
		echo $pt_name= $row['name'];
		echo $pt_email=$row['email'];
		echo $pt_mobile=$row['mobile'];



	 	$query = 'SELECT gcm_regId from hospital.doctor where dr_id='. '' .$dr_id;
		
		
		$result = $this->db->query($query);
		$row = $result->fetch_assoc();
		$regId= $row['gcm_regId'];
////////////////*****************************


	 $firebase = new Firebase();
        $push = new Push();
 
        

        // optional payload
        $payload = array();
        $payload['team'] = 'India';
        $payload['score'] = '5.6';
 
        // notification title
      //  $setTitle = isset($_GET['title']) ? $_GET['title'] : '';

         $title = 'Test';
         
        // notification message
        // $message = isset($_GET['message']) ? $_GET['message'] : '';
         $message = 'User adde Successfully';
         
        // push type - single user / topic
        // $push_type = isset($_GET['push_type']) ? $_GET['push_type'] : '';
         $push_type = 'individual';
         
        // whether to include to image or not
        // $include_image = isset($_GET['include_image']) ? TRUE : FALSE;
          $include_image = true;
 
 
        $push->setTitle($title);
        $push->setMessage($message);
        if ($include_image) {
            $push->setImage('http://api.androidhive.info/images/minion.jpg');
        } else {
            $push->setImage('');
        }
        $push->setIsBackground(FALSE);
        $push->setPayload($payload);
 
 
        $json = '';
        $response = '';
 
        if ($push_type == 'topic') {
            $json = $push->getPush();
            $response = $firebase->sendToTopic('global', $json);
        } else if ($push_type == 'individual') {
            $json = $push->getPush();
           // $regId = isset($_GET['regId']) ? $_GET['regId'] : '';

            $response = $firebase->send($regId, $json);
        }




	///////////////////////////////////////////////******************************



		
	}

		//echo $result;
			mysqli_close($con);
		return $list;
	}


	function getAllDoctorsByDeptID($data)
	{


		$dp_id = $data['dp_id'];
		$query = 'SELECT * from hospital.doctor where dp_id='. '' .$dp_id;
		
		$list = array();
		$result = $this->db->query($query);
		while ($row = $result->fetch_assoc())
		{
			$list[] = $row;
		}
		return $list;
	}


	function getAllAppointmentByDrID($data)
	{


		$dr_id = $data['dr_id'];
		 $query = 'SELECT * from hospital.appointment where dr_id='. '' .$dr_id;
		
		$list = array();
		$result = $this->db->query($query);
		while ($row = $result->fetch_assoc())
		{
			$list[] = $row;
		}
		return $list;
	}




	



	function getAllDep()
	{


	
		$query = 'SELECT * from hospital.department';
		
		$list = array();
		$result = $this->db->query($query);
		while ($row = $result->fetch_assoc())
		{
			$list[] = $row;
		}
		return $list;
	}
}
